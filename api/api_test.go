package api

import "testing"

func Test_pathEscape(t *testing.T) {
	tests := []struct {
		name string
		raw  string
		want string
	}{
		{
			name: "POSITIVE - escapes elements between '/' - nothing needs escaping - removes leading '/'",
			raw:  "/device/1234/overview",
			want: "device/1234/overview",
		},
		{
			name: "POSITIVE - escapes elements between '/'",
			raw:  "device/12,34/overview",
			want: "device/12%2C34/overview",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := pathEscape(tt.raw); got != tt.want {
				t.Errorf("pathEscape() = %v, want %v", got, tt.want)
			}
		})
	}
}
