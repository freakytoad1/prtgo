package prtgo

import (
	"crypto/tls"
	"net/http"
	"net/url"
	"reflect"
	"testing"
	"time"

	"gitlab.com/freakytoad1/prtgo/api"
)

func TestClient_String(t *testing.T) {
	t1, _ := time.Parse(time.RFC3339, "2022-03-18T23:17:01Z")
	type fields struct {
		token           string
		tokenExpiration time.Time
		username        string
		password        string
	}
	tests := []struct {
		name   string
		fields fields
		want   string
	}{
		{
			name: "POSITIVE - does not print pass or token",
			fields: fields{
				token:           "1234",
				tokenExpiration: t1,
				username:        "freakytoad1",
				password:        "5678",
			},
			want: "Username: freakytoad1 Password: **** Token: **** Token Expiration: 2022-03-18 23:17:01 +0000 UTC",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := &Client{
				token:           tt.fields.token,
				tokenExpiration: tt.fields.tokenExpiration,
				username:        tt.fields.username,
				password:        tt.fields.password,
			}
			if got := c.String(); got != tt.want {
				t.Errorf("Client.String() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNew(t *testing.T) {
	type args struct {
		core    string
		user    string
		pass    string
		options []Option
	}
	tests := []struct {
		name    string
		args    args
		want    *Client
		wantErr bool
	}{
		{
			name: "POSITIVE - passes validation with IP",
			args: args{
				core: "192.0.2.1",
				user: "freakytoad1",
				pass: "1234",
			},
			want: &Client{
				username: "freakytoad1",
				password: "1234",
				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
					},
					Url: url.URL{
						Scheme: "https",
						Host:   "192.0.2.1:8443",
						Path:   "/api/v2/",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "POSITIVE - passes validation with hostname",
			args: args{
				core: "prtg.com",
				user: "freakytoad1",
				pass: "1234",
			},
			want: &Client{
				username: "freakytoad1",
				password: "1234",
				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
					},
					Url: url.URL{
						Scheme: "https",
						Host:   "prtg.com:8443",
						Path:   "/api/v2/",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "POSITIVE - passes validation with subdomain hostname",
			args: args{
				core: "www.prtg.com",
				user: "freakytoad1",
				pass: "1234",
			},
			want: &Client{
				username: "freakytoad1",
				password: "1234",
				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
					},
					Url: url.URL{
						Scheme: "https",
						Host:   "www.prtg.com:8443",
						Path:   "/api/v2/",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "POSITIVE - with custom timeout",
			args: args{
				core:    "192.0.2.1",
				user:    "freakytoad1",
				pass:    "1234",
				options: []Option{WithTimeout(time.Second * 60)},
			},
			want: &Client{
				username: "freakytoad1",
				password: "1234",
				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 60,
					},
					Url: url.URL{
						Scheme: "https",
						Host:   "192.0.2.1:8443",
						Path:   "/api/v2/",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "POSITIVE - with tls skip verify",
			args: args{
				core:    "192.0.2.1",
				user:    "freakytoad1",
				pass:    "1234",
				options: []Option{WithInsecureSkipVerify()},
			},
			want: &Client{
				username: "freakytoad1",
				password: "1234",
				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
						Transport: &http.Transport{
							TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
						},
					},
					Url: url.URL{
						Scheme: "https",
						Host:   "192.0.2.1:8443",
						Path:   "/api/v2/",
					},
				},
			},
			wantErr: false,
		},
		{
			name: "NEGATIVE - fails validation with url",
			args: args{
				core: "https://www.prtg.com", // api client handles adding the schema
				user: "freakytoad1",
				pass: "1234",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "NEGATIVE - fails validation with no user",
			args: args{
				core: "prtg.com",
				user: "",
				pass: "1234",
			},
			want:    nil,
			wantErr: true,
		},
		{
			name: "NEGATIVE - fails validation with no pass",
			args: args{
				core: "prtg.com",
				user: "freakytoad1",
				pass: "",
			},
			want:    nil,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := New(tt.args.core, tt.args.user, tt.args.pass, tt.args.options...)
			if (err != nil) != tt.wantErr {
				t.Errorf("New() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("New() = %v, want %v", got, tt.want)
			}
		})
	}
}
