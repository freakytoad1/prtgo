# prtGo [![pipeline status](https://gitlab.com/freakytoad1/prtgo/badges/main/pipeline.svg)](https://gitlab.com/freakytoad1/prtgo/-/commits/main) [![coverage report](https://gitlab.com/freakytoad1/prtgo/badges/main/coverage.svg)](https://gitlab.com/freakytoad1/prtgo/-/commits/main)

Wrapper to interact with PRTG's v2 API. The API is in alpha at the moment so it is subject to change heavily. No guarantee this code will continue to work with newer versions.

## Official PRTG v2 API Docs

[Overview](https://www.paessler.com/support/prtg/api/v2/overview)

[Reference Guide](https://www.paessler.com/support/prtg/api/v2/oas)

## Index
- [Making Client](#making-client)
- [Devices](#devices)
  - [Get Devices](#get-devices)
  - [Get Device by ID](#get-device-by-id)

### Making Client
The PRTGO Client represents a way to interact with PRTG's v2 API. The client stores the PRTG Core address, username, and generated token. All methods hang off the client.

Minimum Requirements:
- PRTG Core hostname or IP
- Username
- Password

Additional Options to pass to `New()`:
- `WithTimeout(time.Duration)` allows custom http client timeout
- `WithInsecureSkipVerify()` allows server certificates to be ignored

<details>
  <summary>Minimum Example</summary>

```go
import "gitlab.com/freakytoad/prtgo"

func main() {
    // PRTG Core hostname/ip, username, and password are the minimum requirements.
    prtgClient, err := prtgo.New("prtg.freakytoad1.com", "username", "password123")
    if err != nil {
        log.Fatalf("unable to make new prtg client: %v", err)
    }

    // Use prtgClient.xxx() methods ...
}
```

</details>

<details>
  <summary>Custom Timeout Example</summary>
  
```go
import "gitlab.com/freakytoad/prtgo"

func main() {
    // PRTG Core hostname/ip, username, and password are the minimum requirements.
    // WithTimeout is optional
    prtgClient, err := prtgo.New("prtg.freakytoad1.com", "username", "password123", prtgo.WithTimeout(1 * time.Minute))
    if err != nil {
        log.Fatalf("unable to make new prtg client: %v", err)
    }

    // Use prtgClient.xxx() methods ...
}
```

</details>

<details>
  <summary>Ignore Certificates Example</summary>
  
```go
import "gitlab.com/freakytoad/prtgo"

func main() {
    // PRTG Core hostname/ip, username, and password are the minimum requirements.
    // WithInsecureSkipVerify is optional
    prtgClient, err := prtgo.New("prtg.freakytoad1.com", "username", "password123", prtgo.WithInsecureSkipVerify())
    if err != nil {
        log.Fatalf("unable to make new prtg client: %v", err)
    }

    // Use prtgClient.xxx() methods ...
}
```

</details>

### Devices

### Get Devices
Returns details on all devices on the core

#### Return 
`[]*prtgo.Device`

```go
type Device struct {
	Autodiscovery      *AutoDiscovery `json:"autodiscovery"`
	Dependency         *Dependency    `json:"dependency"`
	Favorite           bool           `json:"favorite"`
	Host               string         `json:"host"`
	Icon               string         `json:"icon"`
	ID                 string         `json:"id"`
	LastRecommendation time.Time      `json:"last_recommendation"` // RFC3339 formatted time in UTC+0 timezone - 2022-03-18T23:17:01Z
	Location           *Location      `json:"location"`
	Name               string         `json:"name"`
	Path               []*Path        `json:"path"`
	Permissions        int64          `json:"permissions"`
	Position           int64          `json:"position"`
	Priority           int64          `json:"priority"`
	ScanningInterval   int64          `json:"scanning_interval"`
	ScanningTimes      []interface{}  `json:"scanning_times"` // unsure what these interfaces will be
	Self               *Self          `json:"self"`
	SensorSummary      *SensorSummary `json:"sensor_summary"`
	Status             string         `json:"status"`
	StatusInfo         interface{}    `json:"status_info"` // unsure what these interfaces will be
}
```

<details>
  <summary>Example</summary>
  
```go
import "gitlab.com/freakytoad/prtgo"

func main() {
    // PRTG Core hostname/ip, username, and password are the minimum requirements
    prtgClient, err := prtgo.New("prtg.freakytoad1.com", "username", "password123")
    if err != nil {
        log.Fatalf("unable to make new prtg client: %v", err)
    }

    // Get all the devices on the core
    devices, err := prtgClient.GetDevices()
    if err != nil {
        log.Fatalf("unable to get all devices: %v", err)
    }

    fmt.Prinft("Device 1 name: %s", devices[0].Name)
}
```

</details>

### Get Device by ID
Returns details on a device by the ID passed in.

#### Return 
`*prtgo.Device`

See above for Device fields.

<details>
  <summary>Example</summary>
  
```go
import "gitlab.com/freakytoad/prtgo"

func main() {
    // PRTG Core hostname/ip, username, and password are the minimum requirements
    prtgClient, err := prtgo.New("prtg.freakytoad1.com", "username", "password123")
    if err != nil {
        log.Fatalf("unable to make new prtg client: %v", err)
    }

    // Get de3vice 1234 on core
    id := 1234
    device, err := prtgClient.GetDevice(id)
    if err != nil {
        log.Fatalf("unable to get device [%d]: %v", id, err)
    }

    fmt.Prinft("Device %d name: %s", id, device.Name)
}
```

</details>