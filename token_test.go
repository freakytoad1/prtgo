package prtgo

import (
	"net/http"
	"testing"
	"time"

	"github.com/jarcoal/httpmock"
	"gitlab.com/freakytoad1/prtgo/api"
	"gitlab.com/freakytoad1/prtgo/tools"
)

func Test_isTokenExpired(t *testing.T) {
	tests := []struct {
		name       string
		expiration time.Time
		want       bool
	}{
		// these aren't perfect time tests but they show the function
		// ideally the 'current' time in the function would be mocked so we can set specific times
		{
			name:       "POSITIVE - token has 5 min left",
			expiration: time.Now().Add(5 * time.Minute),
			want:       false,
		},
		{
			name:       "NEGATIVE - token is 5 min old",
			expiration: time.Now().Add(-5 * time.Minute),
			want:       true,
		},
		{
			name:       "NEGATIVE - token has less than 5 sec",
			expiration: time.Now().Add(3 * time.Second),
			want:       true,
		},
		{
			name:       "NEGATIVE - token expiration is now",
			expiration: time.Now(),
			want:       true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isTokenExpired(tt.expiration); got != tt.want {
				t.Errorf("isTokenExpired() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isTokenAlmostExpired(t *testing.T) {
	tests := []struct {
		name       string
		expiration time.Time
		want       bool
	}{
		{
			name:       "POSITIVE - token has 5 min left",
			expiration: time.Now().Add(5 * time.Minute),
			want:       false,
		},
		{
			name:       "NEGATIVE - token less than 1 min left",
			expiration: time.Now().Add(30 * time.Second),
			want:       true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := isTokenAlmostExpired(tt.expiration); got != tt.want {
				t.Errorf("isTokenAlmostExpired() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestClient_getToken(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	// default responder
	httpmock.RegisterNoResponder(httpmock.ConnectionFailure)

	tests := []struct {
		name    string
		c       *Client
		mock    *tools.MockAPIResponse
		wantErr bool
	}{
		{
			name: "POSITIVE - gets token and sets expiration",
			c: &Client{
				username: "freakytoad1",
				password: "1234",

				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
					},
					Url: tools.GetBasePath(),
				},
			},
			mock: tools.NewAPIMock(
				http.MethodPost,
				"session",
				tools.GetBasePath(),
				http.StatusOK,
				tools.GetValidTokenResponse(),
			),
			wantErr: false,
		},
		{
			name: "NEGATIVE - empty token returned",
			c: &Client{
				username: "freakytoad1",
				password: "1234",

				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
					},
					Url: tools.GetBasePath(),
				},
			},
			mock: tools.NewAPIMock(
				http.MethodPost,
				"session",
				tools.GetBasePath(),
				http.StatusOK,
				tools.GetEmptyTokenResponse(),
			),
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock.CreateResponder()
			if err := tt.c.getToken(); (err != nil) != tt.wantErr {
				t.Errorf("Client.getToken() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestClient_refreshToken(t *testing.T) {
	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	// default responder
	httpmock.RegisterNoResponder(httpmock.ConnectionFailure)

	tests := []struct {
		name    string
		c       *Client
		mock    *tools.MockAPIResponse
		wantErr bool
	}{
		{
			name: "POSITIVE - gets token because there is none",
			c: &Client{
				username: "freakytoad1",
				password: "1234",
				token:    "",

				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
					},
					Url: tools.GetBasePath(),
				},
			},
			mock: tools.NewAPIMock(
				http.MethodPost,
				"session",
				tools.GetBasePath(),
				http.StatusOK,
				tools.GetValidTokenResponse(),
			),
			wantErr: false,
		},
		{
			name: "POSITIVE - token exists but is expired, get new one",
			c: &Client{
				username:        "freakytoad1",
				password:        "1234",
				token:           "1234",
				tokenExpiration: time.Now().Add(-5 * time.Minute),

				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
					},
					Url: tools.GetBasePath(),
				},
			},
			mock: tools.NewAPIMock(
				http.MethodPost,
				"session",
				tools.GetBasePath(),
				http.StatusOK,
				tools.GetValidTokenResponse(),
			),
			wantErr: false,
		},
		{
			name: "POSITIVE - token exists but is ALMOST expired, refresh",
			c: &Client{
				username:        "freakytoad1",
				password:        "1234",
				token:           "1234",
				tokenExpiration: time.Now().Add(30 * time.Second),

				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
					},
					Url: tools.GetBasePath(),
				},
			},
			mock: tools.NewAPIMock(
				http.MethodGet,
				"session",
				tools.GetBasePath(),
				http.StatusOK,
				tools.GetEmptyTokenResponse(),
			),
			wantErr: false,
		},
		{
			name: "POSITIVE - token exists and is not expired, do nothing",
			c: &Client{
				username:        "freakytoad1",
				password:        "1234",
				token:           "1234",
				tokenExpiration: time.Now().Add(30 * time.Minute),

				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
					},
					Url: tools.GetBasePath(),
				},
			},
			mock: tools.NewAPIMock( // this mock doesn't matter since the token is fine
				http.MethodGet,
				"session",
				tools.GetBasePath(),
				http.StatusOK,
				"",
			),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock.CreateResponder()
			if err := tt.c.refreshToken(); (err != nil) != tt.wantErr {
				t.Errorf("Client.refreshToken() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
