package prtgo

import (
	"fmt"
	"net/http"
	"time"

	"gitlab.com/freakytoad1/prtgo/api"
)

// returned token struct from PRTG
type token struct {
	ApplicationServerVersion string    `json:"application_server_version"`
	AutomaticLogoutDuration  int64     `json:"automatic_logout_duration"`
	AutomaticLogoutEnabled   bool      `json:"automatic_logout_enabled"`
	ExpiresAt                time.Time `json:"expires_at"` // RFC3339 formatted time in UTC+0 timezone - 2022-03-18T23:17:01Z
	Token                    string    `json:"token"`
	User                     *user     `json:"user"` // only included during new token, not returned during renewal
}

// returned user struct from PRTG.
// included in new token acquisition.
type user struct {
	Active              bool   `json:"active"`
	ActualPermissions   int64  `json:"actual_permissions"`
	AdPath              string `json:"ad_path"`
	AdUser              bool   `json:"ad_user"`
	AudibleAlarm        string `json:"audible_alarm"`
	AutoRefresh         bool   `json:"auto_refresh"`
	AutoRefreshInterval int64  `json:"auto_refresh_interval"`
	DateTimeFormat      string `json:"date_time_format"`
	DisplayName         string `json:"display_name"`
	Email               string `json:"email"`
	GroupFoldSize       int64  `json:"group_fold_size"`
	GroupMembership     []struct {
		Href string `json:"href"`
		ID   string `json:"id"`
		Name string `json:"name"`
		Type string `json:"type"`
	} `json:"group_membership"`
	GroupViewContent string   `json:"group_view_content"`
	GroupViewType    string   `json:"group_view_type"`
	Homepage         string   `json:"homepage"`
	ID               string   `json:"id"`
	LastLogin        string   `json:"last_login"`
	LoginName        string   `json:"login_name"`
	Permissions      []string `json:"permissions"`
	PrimaryGroup     struct {
		Href string `json:"href"`
		ID   string `json:"id"`
		Name string `json:"name"`
		Type string `json:"type"`
	} `json:"primary_group"`
	Role           string `json:"role"`
	SensorFoldSize int64  `json:"sensor_fold_size"`
	Theme          string `json:"theme"`
	TicketEmail    bool   `json:"ticket_email"`
	TimeZone       string `json:"time_zone"`
}

// getToken will grab a new token for this user.
// Tokens expire after 1 hour if they are not renewed.
func (c *Client) getToken() error {
	bdy := struct {
		Username string `json:"username"`
		Password string `json:"password"`
	}{
		Username: c.username,
		Password: c.password,
	}

	// make request
	// use direct api newrequest function since the token isn't in play yet
	req, err := c.NewRequest(http.MethodPost, "/session", api.WithJSONBody(bdy))
	if err != nil {
		return fmt.Errorf("unable to make new token request: %w", err)
	}

	// do and parse the token
	resp := &token{}
	if err := c.DoParse(req, resp); err != nil {
		return fmt.Errorf("unable to parse token response: %w", err)
	}

	// check for token
	if len(resp.Token) < 1 {
		return fmt.Errorf("token was not returned by PRTG")
	}

	// set the token
	c.setTokenValue(resp.Token)

	// set expiration
	c.setTokenExpirationValue(resp.ExpiresAt)

	return nil
}

// renewToken will request that the existing token's expiration be extended
func (c *Client) renewToken() error {
	// make request, don't use wrapper or a circular problem happens
	req, err := c.NewRequest(http.MethodGet, "/session", api.WithToken(c.getTokenValue()))
	if err != nil {
		return fmt.Errorf("unable to make token renew request: %w", err)
	}

	// do and parse the token
	resp := &token{}
	if err := c.DoParse(req, resp); err != nil {
		return fmt.Errorf("unable to parse token renew response: %w", err)
	}

	// set new expiration
	c.setTokenExpirationValue(resp.ExpiresAt)

	return nil
}

// refreshToken will first get a token if there currently isn't one saved.
// If there is one saved and it is expired, it will be refreshed and its expiration will be extended.
func (c *Client) refreshToken() error {
	// no token so we should try to get one first
	if c.getTokenValue() == "" {
		if err := c.getToken(); err != nil {
			return err
		}
	}

	// token exists, check its expiration
	if isTokenExpired(c.getTokenExpirationValue()) {
		// get new token, can't renew an expired one
		if err := c.getToken(); err != nil {
			return err
		}
	} else if isTokenAlmostExpired(c.getTokenExpirationValue()) {
		// renew current token
		if err := c.renewToken(); err != nil {
			return err
		}
	}

	return nil
}

// checks if the token is already expired.
func isTokenExpired(expiration time.Time) bool {
	current := time.Now()

	if expiration == current {
		// if equal, expired
		return true
	} else if expiration.Before(current) {
		// if expiration time is before current time, expired
		return true
	} else if expiration.Sub(current) <= 5*time.Second {
		// within 5 seconds can be considered expired
		return true
	}

	return false
}

// checks if the token is close to expiring
func isTokenAlmostExpired(expiration time.Time) bool {
	// grab current time to compare against
	current := time.Now()

	// less than a minute left, should renew
	if expiration.Sub(current) <= 1*time.Minute {
		return true
	}

	// more than 1 minute left on the token
	return false
}

// safely sets token
func (c *Client) setTokenValue(t string) {
	c.tokenLock.Lock()
	defer c.tokenLock.Unlock()
	c.token = t
}

// safely retrieves token
func (c *Client) getTokenValue() string {
	c.tokenLock.RLock()
	defer c.tokenLock.RUnlock()
	return c.token
}

// safely set expiration
func (c *Client) setTokenExpirationValue(t time.Time) {
	c.tokenLock.Lock()
	defer c.tokenLock.Unlock()
	c.tokenExpiration = t
}

// safely retrieves expiration
func (c *Client) getTokenExpirationValue() time.Time {
	c.tokenLock.RLock()
	defer c.tokenLock.RUnlock()
	return c.tokenExpiration
}
