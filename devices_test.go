package prtgo

import (
	"net/http"
	"reflect"
	"testing"
	"time"

	"github.com/jarcoal/httpmock"
	"gitlab.com/freakytoad1/prtgo/api"
	"gitlab.com/freakytoad1/prtgo/tools"
)

func TestClient_GetDevice(t *testing.T) {
	// parsed out time
	parsedTime, err := time.Parse(time.RFC3339, "2022-03-20T01:49:17Z")
	if err != nil {
		t.Fatalf("couldn't parse time: %v", err)
	}

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	// session responder
	sessMock := tools.NewAPIMock(
		http.MethodPost,
		"session",
		tools.GetBasePath(),
		http.StatusOK,
		tools.GetValidTokenResponse(),
	)
	sessMock.CreateResponder()

	// default responder
	httpmock.RegisterNoResponder(httpmock.ConnectionFailure)

	tests := []struct {
		name    string
		c       *Client
		id      int64
		want    *Device
		mock    *tools.MockAPIResponse
		wantErr bool
	}{
		{
			name: "POSITIVE - gets the device with the id",
			c: &Client{
				username: "freakytoad1",
				password: "1234",
				token:    "1234",

				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
					},
					Url: tools.GetBasePath(),
				},
			},
			id: 1234,
			want: &Device{
				ID:                 "1234",
				Name:               "Device 1",
				Position:           10,
				Favorite:           false,
				Priority:           3,
				Status:             "SUMMED_INFO_UP",
				StatusInfo:         nil,
				Location:           nil,
				Permissions:        63,
				ScanningInterval:   60,
				ScanningTimes:      nil,
				Host:               "192.0.2.1",
				Icon:               "A_Server_1.png",
				LastRecommendation: parsedTime,
				Autodiscovery: &AutoDiscovery{
					Active:            false,
					AutodiscoveryType: "AUTO",
					Schedule:          "ONCE",
					Progress:          0,
					LastAutodiscovery: parsedTime,
				},
				SensorSummary: &SensorSummary{
					Total:              2,
					Down:               0,
					DownAcknowledged:   0,
					Warning:            0,
					Up:                 2,
					PausedByDependency: 0,
					PausedByLicense:    0,
					PausedBySchedule:   0,
					PausedByUser:       0,
					PausedByUserUntil:  0,
					PausedByParent:     0,
					Unusual:            0,
					Unknown:            0,
					DownPartial:        0,
					ProbeDisconnected:  0,
					Collecting:         0,
					None:               0,
				},
				Dependency: &Dependency{
					ID:   "1235",
					Type: "REFERENCED_SENSOR",
					Name: "Ping",
					Href: "/api/v2/sensors/1235",
				},
				Path: []*Path{
					{
						ID:   "0",
						Type: "REFERENCED_ROOT",
						Name: "Root",
						Href: "/api/v2/groups/0",
					},
					{
						ID:   "1",
						Type: "REFERENCED_PROBE",
						Name: "Local Probe",
						Href: "/api/v2/probes/1",
					},
					{
						ID:   "2",
						Type: "REFERENCED_GROUP",
						Name: "Group 2",
						Href: "/api/v2/groups/2",
					},
					{
						ID:   "3",
						Type: "REFERENCED_GROUP",
						Name: "Group 3",
						Href: "/api/v2/groups/3",
					},
				},
				Self: &Self{
					ID:   "1234",
					Type: "REFERENCED_DEVICE",
					Name: "Device 1",
					Href: "/api/v2/devices/1234",
				},
			},
			mock: tools.NewAPIMock(
				http.MethodGet,
				"devices/1234/overview",
				tools.GetBasePath(),
				http.StatusOK,
				tools.GetGoodDeviceResponse(1234),
			),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock.CreateResponder()
			got, err := tt.c.GetDevice(tt.id)
			if (err != nil) != tt.wantErr {
				t.Errorf("Client.GetDevice() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Client.GetDevice() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestClient_GetDevices(t *testing.T) {
	// parsed out time
	parsedTime, err := time.Parse(time.RFC3339, "2022-03-20T01:49:17Z")
	if err != nil {
		t.Fatalf("couldn't parse time: %v", err)
	}

	httpmock.Activate()
	defer httpmock.DeactivateAndReset()

	// session responder
	sessMock := tools.NewAPIMock(
		http.MethodPost,
		"session",
		tools.GetBasePath(),
		http.StatusOK,
		tools.GetValidTokenResponse(),
	)
	sessMock.CreateResponder()

	// default responder
	httpmock.RegisterNoResponder(httpmock.ConnectionFailure)

	tests := []struct {
		name    string
		c       *Client
		want    []*Device
		mock    *tools.MockAPIResponse
		wantErr bool
	}{
		{
			name: "POSITIVE - gets all devices",
			c: &Client{
				username: "freakytoad1",
				password: "1234",
				token:    "1234",

				Client: &api.Client{
					Client: &http.Client{
						Timeout: time.Second * 30,
					},
					Url: tools.GetBasePath(),
				},
			},
			mock: tools.NewAPIMock(
				http.MethodGet,
				"devices",
				tools.GetBasePath(),
				http.StatusOK,
				"["+tools.GetGoodDeviceResponse(1234)+","+tools.GetGoodDeviceResponse(1235)+"]",
			),
			wantErr: false,
			want: []*Device{
				{
					ID:                 "1234",
					Name:               "Device 1",
					Position:           10,
					Favorite:           false,
					Priority:           3,
					Status:             "SUMMED_INFO_UP",
					StatusInfo:         nil,
					Location:           nil,
					Permissions:        63,
					ScanningInterval:   60,
					ScanningTimes:      nil,
					Host:               "192.0.2.1",
					Icon:               "A_Server_1.png",
					LastRecommendation: parsedTime,
					Autodiscovery: &AutoDiscovery{
						Active:            false,
						AutodiscoveryType: "AUTO",
						Schedule:          "ONCE",
						Progress:          0,
						LastAutodiscovery: parsedTime,
					},
					SensorSummary: &SensorSummary{
						Total:              2,
						Down:               0,
						DownAcknowledged:   0,
						Warning:            0,
						Up:                 2,
						PausedByDependency: 0,
						PausedByLicense:    0,
						PausedBySchedule:   0,
						PausedByUser:       0,
						PausedByUserUntil:  0,
						PausedByParent:     0,
						Unusual:            0,
						Unknown:            0,
						DownPartial:        0,
						ProbeDisconnected:  0,
						Collecting:         0,
						None:               0,
					},
					Dependency: &Dependency{
						ID:   "1235",
						Type: "REFERENCED_SENSOR",
						Name: "Ping",
						Href: "/api/v2/sensors/1235",
					},
					Path: []*Path{
						{
							ID:   "0",
							Type: "REFERENCED_ROOT",
							Name: "Root",
							Href: "/api/v2/groups/0",
						},
						{
							ID:   "1",
							Type: "REFERENCED_PROBE",
							Name: "Local Probe",
							Href: "/api/v2/probes/1",
						},
						{
							ID:   "2",
							Type: "REFERENCED_GROUP",
							Name: "Group 2",
							Href: "/api/v2/groups/2",
						},
						{
							ID:   "3",
							Type: "REFERENCED_GROUP",
							Name: "Group 3",
							Href: "/api/v2/groups/3",
						},
					},
					Self: &Self{
						ID:   "1234",
						Type: "REFERENCED_DEVICE",
						Name: "Device 1",
						Href: "/api/v2/devices/1234",
					},
				},
				{
					ID:                 "1235",
					Name:               "Device 1",
					Position:           10,
					Favorite:           false,
					Priority:           3,
					Status:             "SUMMED_INFO_UP",
					StatusInfo:         nil,
					Location:           nil,
					Permissions:        63,
					ScanningInterval:   60,
					ScanningTimes:      nil,
					Host:               "192.0.2.1",
					Icon:               "A_Server_1.png",
					LastRecommendation: parsedTime,
					Autodiscovery: &AutoDiscovery{
						Active:            false,
						AutodiscoveryType: "AUTO",
						Schedule:          "ONCE",
						Progress:          0,
						LastAutodiscovery: parsedTime,
					},
					SensorSummary: &SensorSummary{
						Total:              2,
						Down:               0,
						DownAcknowledged:   0,
						Warning:            0,
						Up:                 2,
						PausedByDependency: 0,
						PausedByLicense:    0,
						PausedBySchedule:   0,
						PausedByUser:       0,
						PausedByUserUntil:  0,
						PausedByParent:     0,
						Unusual:            0,
						Unknown:            0,
						DownPartial:        0,
						ProbeDisconnected:  0,
						Collecting:         0,
						None:               0,
					},
					Dependency: &Dependency{
						ID:   "1235",
						Type: "REFERENCED_SENSOR",
						Name: "Ping",
						Href: "/api/v2/sensors/1235",
					},
					Path: []*Path{
						{
							ID:   "0",
							Type: "REFERENCED_ROOT",
							Name: "Root",
							Href: "/api/v2/groups/0",
						},
						{
							ID:   "1",
							Type: "REFERENCED_PROBE",
							Name: "Local Probe",
							Href: "/api/v2/probes/1",
						},
						{
							ID:   "2",
							Type: "REFERENCED_GROUP",
							Name: "Group 2",
							Href: "/api/v2/groups/2",
						},
						{
							ID:   "3",
							Type: "REFERENCED_GROUP",
							Name: "Group 3",
							Href: "/api/v2/groups/3",
						},
					},
					Self: &Self{
						ID:   "1235",
						Type: "REFERENCED_DEVICE",
						Name: "Device 1",
						Href: "/api/v2/devices/1235",
					},
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.mock.CreateResponder()
			got, err := tt.c.GetDevices()
			if (err != nil) != tt.wantErr {
				t.Errorf("Client.GetDevices() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Client.GetDevices() = %v, want %v", got, tt.want)
			}
		})
	}
}
