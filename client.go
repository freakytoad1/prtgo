package prtgo

import (
	"crypto/tls"
	"fmt"
	"net/http"
	"sync"
	"time"

	"github.com/go-playground/validator"
	"gitlab.com/freakytoad1/prtgo/api"
)

type Client struct {
	*api.Client

	// Authorization Bearer token for making authenticated requests.
	// Time at which the current token expires in RFC3339 UTC+0 format.
	token           string `json:"-"`
	tokenExpiration time.Time
	tokenLock       sync.RWMutex

	// passwords are required for prtg core accounts
	username string
	password string `json:"-"`
}

// internal client for validating
// the validator only works on exported fields
type client struct {
	Core     string `validate:"required,hostname|ip"`
	Username string `validate:"required"`
	Password string `validate:"required"`
}

// Option Pattern
type Option func(*Client)

func New(core, user, pass string, options ...Option) (*Client, error) {
	c := &client{
		Core:     core,
		Username: user,
		Password: pass,
	}

	// Check inputs for validation
	if err := c.validate(); err != nil {
		return nil, err
	}

	// All validated, make real Client and perform options
	cli := &Client{
		username: user,
		password: pass,
		Client:   api.New(core),
	}

	for _, option := range options {
		option(cli)
	}

	// All good, return client
	return cli, nil
}

// Connect attempts to grab a token from PRTG.
// This is unneccesary to perform unless you want to verify connectivity.
// Each http request handles getting and renewing the token.
func (c *Client) Connect() error {
	return c.refreshToken()
}

func (c *client) validate() error {
	validate := validator.New()
	if err := validate.Struct(c); err != nil {
		return fmt.Errorf("new client valdiation failed: %w", err)
	}

	return nil
}

func (c *Client) String() string {
	return fmt.Sprintf("Username: %s Password: **** Token: **** Token Expiration: %s", c.username, c.tokenExpiration)
}

// Http timeout
func WithTimeout(timeout time.Duration) Option {
	return func(c *Client) {
		c.Client.Timeout = timeout
	}
}

// For ignoring certs. Only advisable for testing.
// See https://pkg.go.dev/crypto/tls#Config.InsecureSkipVerify for more info
func WithInsecureSkipVerify() Option {
	return func(c *Client) {
		c.Client.Transport = &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		}
	}
}

// newHttpRequest wraps the api package's NewRequest function by first checking token expiration and grabbing a new token if needed.
// This function always adds the WithToken() api.Option to any passed in options.
func (c *Client) newHttpRequest(method, path string, options ...api.Option) (*http.Request, error) {
	if err := c.refreshToken(); err != nil {
		return nil, err
	}

	options = append(options, api.WithToken(c.getTokenValue()))

	return c.NewRequest(method, path, options...)
}
