package tools

import (
	"fmt"
	"time"
)

// future RFC3339 format
func GetFutureTime() string {
	return time.Now().Add(5 * time.Minute).Format(time.RFC3339)
}

// past RFC3339 format
func GetPastTime() string {
	return "2004-01-17T11:59:59Z"
}

func GetValidTokenResponse() string {
	return fmt.Sprintf(`{
		"token": "1234",
		"expires_at": "%s"
	}`, GetFutureTime())
}

// when renewing token, a new token is NOT returned
func GetEmptyTokenResponse() string {
	return fmt.Sprintf(`{
		"token": "",
		"expires_at": "%s"
	}`, GetFutureTime())
}

func GetGoodDeviceResponse(id int64) string {
	return fmt.Sprintf(`{
		"id": "%d",
		"name": "Device 1",
		"position": 10,
		"favorite": false,
		"priority": 3,
		"status": "SUMMED_INFO_UP",
		"status_info": null,
		"location": null,
		"permissions": 63,
		"scanning_interval": 60,
		"scanning_times": null,
		"host": "192.0.2.1",
		"icon": "A_Server_1.png",
		"last_recommendation": "2022-03-20T01:49:17Z",
		"autodiscovery": {
			"active": false,
			"autodiscovery_type": "AUTO",
			"schedule": "ONCE",
			"progress": 0,
			"last_autodiscovery": "2022-03-20T01:49:17Z"
		},
		"sensor_summary": {
			"total": 2,
			"down": 0,
			"down_acknowledged": 0,
			"warning": 0,
			"up": 2,
			"paused_by_dependency": 0,
			"paused_by_license": 0,
			"paused_by_schedule": 0,
			"paused_by_user": 0,
			"paused_by_user_until": 0,
			"paused_by_parent": 0,
			"unusual": 0,
			"unknown": 0,
			"down_partial": 0,
			"probe_disconnected": 0,
			"collecting": 0,
			"none": 0
		},
		"dependency": {
			"id": "1235",
			"type": "REFERENCED_SENSOR",
			"name": "Ping",
			"href": "/api/v2/sensors/1235"
		},
		"path": [
			{
				"id": "0",
				"type": "REFERENCED_ROOT",
				"name": "Root",
				"href": "/api/v2/groups/0"
			},
			{
				"id": "1",
				"type": "REFERENCED_PROBE",
				"name": "Local Probe",
				"href": "/api/v2/probes/1"
			},
			{
				"id": "2",
				"type": "REFERENCED_GROUP",
				"name": "Group 2",
				"href": "/api/v2/groups/2"
			},
			{
				"id": "3",
				"type": "REFERENCED_GROUP",
				"name": "Group 3",
				"href": "/api/v2/groups/3"
			}
		],
		"self": {
			"id": "%d",
			"type": "REFERENCED_DEVICE",
			"name": "Device 1",
			"href": "/api/v2/devices/%d"
		}
	}`, id, id, id)
}
