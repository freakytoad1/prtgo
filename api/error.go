package api

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
)

// Codes info: https://www.paessler.com/support/prtg/api/v2/overview#error_codes
type apiError struct {
	Code      string `json:"code"`       // identifies the type of error that occurred
	Message   string `json:"message"`    // short description of the error in English
	RequestID string `json:"request_id"` // ID of the request that encountered the error. The ID can be used to link the error with messages found in the log files.
}

func (e *apiError) Error() string {
	return fmt.Sprintf("Code: %s RequestID: %s Message: %s", e.Code, e.RequestID, e.Message)
}

func newAPIError(respBody io.ReadCloser) error {
	// read in the body
	body, err := ioutil.ReadAll(respBody)
	if err != nil {
		return fmt.Errorf("unable to read body from prtg error response: %w", err)
	}

	// unmarshal response
	apiErrResp := &apiError{}
	if err := json.Unmarshal(body, apiErrResp); err != nil {
		return fmt.Errorf("unable to unmarshal body from prtg error response: %w", err)
	}

	return apiErrResp
}
