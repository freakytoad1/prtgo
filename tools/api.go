package tools

import (
	"net/url"
	"path"

	"github.com/jarcoal/httpmock"
)

type MockAPIResponse struct {
	Method       string
	Path         string
	ResponseCode int
	Response     string
}

// NewAPIMock returns a struct containing the correct information to run during testing.
// Serve up the endpoint with CreateResponder() during each test.
func NewAPIMock(method, extendedPath string, basePath url.URL, responseCode int, response string) *MockAPIResponse {
	return &MockAPIResponse{
		Method:       method,
		ResponseCode: responseCode,
		Response:     response,
		Path:         path.Join(basePath.Path, extendedPath),
	}
}

// CreateResponder actually registers and runs the mock endpoint.
func (c *MockAPIResponse) CreateResponder() {
	httpmock.RegisterResponder(
		c.Method,
		c.Path,
		httpmock.NewStringResponder(
			c.ResponseCode,
			c.Response,
		),
	)
}

// GetBasePath returns a url object with the base prtg details.
func GetBasePath() url.URL {
	return url.URL{
		Scheme: "https",
		Host:   "prtg.example.com:8443",
		Path:   "/api/v2/",
	}
}
