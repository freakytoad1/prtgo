package api

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type Client struct {
	*http.Client
	Url url.URL
}

// New creates a new *Client which contains the standard *http.Client and the PRTG Core URL.
// Default timeout is set to 30s and can be overridden with options from prtgo.Client.
func New(host string) *Client {
	return &Client{
		Client: &http.Client{
			Timeout: time.Second * 30,
		},
		Url: url.URL{
			Scheme: "https",        // force https for now
			Host:   host + ":8443", // new api on 8443
			Path:   "/api/v2/",
		},
	}
}

// NewRequest wraps http.NewRequest but adds the ability to supply options as needed.
func (c *Client) NewRequest(method string, path string, options ...Option) (*http.Request, error) {
	// escape path and add to base
	u := c.Url
	u.Path = u.Path + pathEscape(path)

	// craft base request
	req, err := http.NewRequest(method, u.String(), nil)
	if err != nil {
		return nil, err
	}

	// perform options on request
	for _, option := range options {
		if err := option(req); err != nil {
			return nil, err
		}
	}

	return req, nil
}

// Option Pattern
type Option func(*http.Request) error

// WithJSONBody allows a new request to include a JSON body.
func WithJSONBody(b interface{}) Option {
	return func(r *http.Request) error {
		// convert struct to bytes
		body, err := json.Marshal(b)
		if err != nil {
			return fmt.Errorf("unable to marshal request body: %w", err)
		}

		// add to request
		r.Header.Add("Content-Type", "application/json")
		r.Body = io.NopCloser(bytes.NewBuffer(body))
		return nil
	}
}

// WithToken puts the token in an Authorization Header
func WithToken(token string) Option {
	return func(r *http.Request) error {
		r.Header.Add("Authorization", fmt.Sprintf("Bearer %s", token))
		return nil
	}
}

// WithQuery adds the key value pairs as a raw query to the request.
func WithQuery(queryMap map[string]string) Option {
	return func(r *http.Request) error {
		params := url.Values{}

		// add each map key / value as a query param
		for key, value := range queryMap {
			params.Add(key, value)
		}

		// encode the values and add to url
		r.URL.RawQuery = params.Encode()
		return nil
	}
}

// DoParse performs standard Do on the request and unmarshals the response body into ans.
// The response is also validated against known PRTG error codes and will provide more insight into why a request may have failed.
func (c *Client) DoParse(req *http.Request, ans interface{}) error {
	// send it
	resp, err := c.Do(req)
	if err != nil {
		return err
	}

	// validate response first
	if err := c.validateResponse(resp); err != nil {
		return err
	}

	// Look at body
	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	// unmarshal response
	if err := json.Unmarshal(body, ans); err != nil {
		return err
	}

	return nil
}

// validateResponse determines if PRTG returned an error from a request.
func (c *Client) validateResponse(resp *http.Response) error {
	if resp.StatusCode < 400 {
		// all good, no error
		return nil
	}

	return newAPIError(resp.Body)
}

// pathEscape parse the input by '/' and escapes the elements.
// the return is an escaped path leaving '/' intact between the elements.
// escaped elements are ',' '?' ';'
func pathEscape(raw string) string {
	parsed := strings.Split(raw, "/")
	var escaped []string

	for _, split := range parsed {
		if split == "" {
			// ignore empties
			continue
		}
		escaped = append(escaped, url.PathEscape(split))
	}

	return strings.Join(escaped, "/")
}
