package prtgo

import (
	"fmt"
	"net/http"
	"time"
)

// as returned by prtg
// 'lastRecommendation' should parse as RFC3339
type Device struct {
	Autodiscovery      *AutoDiscovery `json:"autodiscovery"`
	Dependency         *Dependency    `json:"dependency"`
	Favorite           bool           `json:"favorite"`
	Host               string         `json:"host"`
	Icon               string         `json:"icon"`
	ID                 string         `json:"id"`
	LastRecommendation time.Time      `json:"last_recommendation"` // RFC3339 formatted time in UTC+0 timezone - 2022-03-18T23:17:01Z
	Location           *Location      `json:"location"`
	Name               string         `json:"name"`
	Path               []*Path        `json:"path"`
	Permissions        int64          `json:"permissions"`
	Position           int64          `json:"position"`
	Priority           int64          `json:"priority"`
	ScanningInterval   int64          `json:"scanning_interval"`
	ScanningTimes      []interface{}  `json:"scanning_times"` // unsure what these interfaces will be
	Self               *Self          `json:"self"`
	SensorSummary      *SensorSummary `json:"sensor_summary"`
	Status             string         `json:"status"`
	StatusInfo         interface{}    `json:"status_info"` // unsure what these interfaces will be
}

type AutoDiscovery struct {
	Active            bool      `json:"active"`
	AutodiscoveryType string    `json:"autodiscovery_type"`
	LastAutodiscovery time.Time `json:"last_autodiscovery"` // RFC3339 formatted time in UTC+0 timezone - 2022-03-18T23:17:01Z
	Progress          int64     `json:"progress"`
	Schedule          string    `json:"schedule"`
}

type Dependency struct {
	Href string `json:"href"`
	ID   string `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
}

type Location struct {
	GeoHash     string `json:"geo_hash"`
	GeoPosition struct {
		Latitude  float64 `json:"latitude"`
		Longitude float64 `json:"longitude"`
	} `json:"geo_position"`
	Value string `json:"value"`
}

type Path struct {
	Href string `json:"href"`
	ID   string `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
}

type Self struct {
	Href string `json:"href"`
	ID   string `json:"id"`
	Name string `json:"name"`
	Type string `json:"type"`
}

type SensorSummary struct {
	Collecting         int64 `json:"collecting"`
	Down               int64 `json:"down"`
	DownAcknowledged   int64 `json:"down_acknowledged"`
	DownPartial        int64 `json:"down_partial"`
	None               int64 `json:"none"`
	PausedByDependency int64 `json:"paused_by_dependency"`
	PausedByLicense    int64 `json:"paused_by_license"`
	PausedByParent     int64 `json:"paused_by_parent"`
	PausedBySchedule   int64 `json:"paused_by_schedule"`
	PausedByUser       int64 `json:"paused_by_user"`
	PausedByUserUntil  int64 `json:"paused_by_user_until"`
	ProbeDisconnected  int64 `json:"probe_disconnected"`
	Total              int64 `json:"total"`
	Unknown            int64 `json:"unknown"`
	Unusual            int64 `json:"unusual"`
	Up                 int64 `json:"up"`
	Warning            int64 `json:"warning"`
}

// GetDevices returns all devices on the core.
// TODO: Allow query parameters like filter, sort_by, etc.
func (c *Client) GetDevices() ([]*Device, error) {
	// make request
	req, err := c.newHttpRequest(http.MethodGet, "/devices")
	if err != nil {
		return nil, fmt.Errorf("unable to make new devices request: %w", err)
	}

	// do and parse the response
	resp := &[]*Device{}
	if err := c.DoParse(req, resp); err != nil {
		return nil, fmt.Errorf("unable to parse devices response: %w", err)
	}

	// return the devices
	return *resp, nil
}

// GetDevice returns the device with the provided id.
// There are no additional parameters for this. Sensor Summary is included by default.
func (c *Client) GetDevice(id int64) (*Device, error) {
	// make request
	req, err := c.newHttpRequest(http.MethodGet, fmt.Sprintf("/devices/%d/overview", id))
	if err != nil {
		return nil, fmt.Errorf("unable to make new device request: %w", err)
	}

	// do and parse the response
	resp := &Device{}
	if err := c.DoParse(req, resp); err != nil {
		return nil, fmt.Errorf("unable to parse device response: %w", err)
	}

	// return the device
	return resp, nil
}
